.git_template
=============

Installation
------------

```
git clone https://bitbucket.com/sgur/git_template.git ~/.git_template
```


Configration
------------

### Defaults

```sh
git config --global include.path '~/.git_template/gitconfig'
git config --global init.templatedir '~/.git_template/init.templatedir'
git config --global commit.template '~/.git_template/git_commit_msg.txt'
git config --global core.excludesfile '~/.git_template/gitignore_global.txt'
```

Windows additions:

```sh
git config --global include.path '~/.git_template/gitconfig.windows'
```

or place lines at the top of `~/.gitconfig`

```
[init]
	templatedir = ~/.git_template/init.templatedir
[include]
	path = ~/.git_template/gitconfig
	path = ~/.git_template/gitconfig.windows
```
reference
---------

[Effortless Ctags with Git](http://tbaggery.com/2011/08/08/effortless-ctags-with-git.html)
